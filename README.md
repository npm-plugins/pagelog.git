# pagelog

#### 介绍

* 将可JSON序列化数据打印到指定JSON解析页面，方便阅览、代码收缩、下载、复制、修改等功能集成。
* 主要用于打印api接口返回的超大数据集合，用于便捷分析、识别和提取。
* 支持打印数据类型 object(非function), array, string, number, boolean，不支持打印函数类、dom类、window等实例对象。
* 如果pagelog-webpack-plugin插件服务没有启动，数据将默认使用console.log打印到控制台。在生产环境下插件使用console.log打印。

#### 软件架构
* 必须配和  [pagelog-webpack-plugin](https://www.npmjs.com/package/pagelog-webpack-plugin) 插件使用，该插件用于预先启动json解析页面http代理服务和数据传输服务。
* 如遇到typescript项目使用，需要声明d.ts类型。

#### 安装

1.  npm install pagelog --save
2.  npm install pagelog-webpack-plugin --save-dev

#### API: pagelog

* wsPort: string || number
    socket服务端口号， default: 11996
    
* clear: boolean

    false: 关闭后保留之前的log打印。default: true

#### API: pagelog-webpack-plugin

* wsPort: string || number

    socket服务端口号， default: 11996

* htmlPort: string || number

    html打印服务端口号， default: 51996
    
* forceRun: boolean

    true: 开启后重新编译后强制重启服务。default: false

#### 使用说明

* 1、webpack插件配置，只需在development环境下启动插件。

    * 用于先启动json解析页面http代理服务和数据传输服务。
    * PageLog页面服务默认访问地址：[http://localhost:51996](http://localhost:51996)
    * 后台socket数据互动服务默认占用端口号：11996

> webpack.config.js  
```javascript
const PageLogWebpackPlugin = require('pagelog-webpack-plugin');
// 默认使用
module.exports = {
    plugins: [
      new PageLogWebpackPlugin()
    ]
}
// 配置式
module.exports = {
    plugins: [
      new PageLogWebpackPlugin({
          wsPort: 11996,  // socket服务端口号，需要和pagelog配置一致
          htmlPort: 51996,// html页面访问端口号
      })
    ]
}

```

* 2、简单使用：在项目入口引入pagelog
> src/index.js
```javascript
// 简单引入
import 'pagelog';

// 简单引入--直接使用
let info = new String() || new Array() || new Boolean() || new Object() || new Number();// 支持数据类型 object(非function), array, string, number, boolean
console.plog(info);
```

* 3、可配置使用： 在项目入口引入pagelog
> src/index.js
```javascript
// 可配置引入
import pageLog from 'pagelog';
pageLog.options({wsPort: 8081}); // 可修改socket服务端口号

// 选择性使用
let info = new String() || new Array() || new Boolean() || new Object() || new Number();// 支持数据类型 object(非function), array, string, number, boolean
console.plog(info);
// 或者 
pageLog.log(info);
// 其他数据打印功能待补充(0_0!)

```

* 4、在typescript项目中的d.ts声明

> types/global.d.ts
```typescript
// 声明 plog方法类
declare interface Console {
  plog: any,
  [propsName: string]: any
}
// 如果使用可配置模式，需要添加如下声明
declare module 'pagelog';
```
  后期补充 @types/pagelog 插件自动声明类型。
