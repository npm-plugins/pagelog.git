import {isPlainObject, isStringNumber} from './utils';
class PageLog {
  constructor() {
    this.wsPort = 11996; // socket服务（由pagelog-webpack-plugin提供）
    this.wsTcp = 'ws://localhost:' + this.wsPort;
    this.clear = true; // 是否清楚log缓存
    this.init();
  }

  /**
   *
   * @param options
   */
  options(options = {}){
    if(isPlainObject(options)){
      if(options.wsPort && isStringNumber(options.wsPort)){
        this.wsPort = parseInt(options.wsPort);
        this.wsTcp = 'ws://localhost:' + this.wsPort;
      }
      if(typeof options['clear'] === 'boolean'){
        this.clear = options['clear'];
      }
    }

  }

  init(){
    if(this.clear){
      this.sendMsg('PAGE_LOG_CLEAR');
    }
  }

  log(...args) {
    // 通过opt处理格式化msg
    this.sendMsg({
      msg: this.handleLog(args),
      opt: {
        // 配置项，待开发
      }
    });
  }

  /**
   * 处理数据结构
   * @param args
   */
  handleLog(args){
    let msgJson = [];
    args.forEach((value, idx) => {
      msgJson.push(value)
    })
    return msgJson;
  }

  events(ws, msg) {
    if(ws){
      ws.onopen = () => {
        // console.log('[PageLog]: 已连接');
        if(typeof msg === "string"){
          ws.send(msg);
        } else {
          ws.send(JSON.stringify(msg));
        }
        ws.close();
      }
      ws.onclose = () => {
        // console.log('[PageLog]: 已断开连接');
      }

      ws.onerror = (err) => {
        console.log('[PageLog]: 无法连接,' + err);
        // 正常打印
        // console.log(msg);
      }
    }else {
      // console.log(msg);
    }
  }

  sendMsg(msg){
    const ws = new WebSocket(this.wsTcp);
    this.events(ws, msg);
  }
}

/**
 * 修饰
 * @param pageLog
 * @returns {*}
 */
function withInit(pageLog) {

  var getStackTrace = function () {
    var obj = {};
    Error.captureStackTrace(obj, getStackTrace);
    return obj.stack;
  };
  window.console.plog = function (...args) {
    console.log(...args);
    var stack = getStackTrace() || "";

    var matchResult = stack.match(/\(.*?\)/g) || []
    var line = matchResult[1] || ""
    args.unshift('Log at:' + line.replace("(", "").replace(")", ""));
    pageLog.log.apply(pageLog, args);
  };

  return pageLog;
}

export default withInit(new PageLog());
