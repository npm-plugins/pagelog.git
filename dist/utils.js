/**
 * 合并配置
 * @param defaultOptions
 * @param options
 * @returns {*}
 */
function mergeOptions(defaultOptions, options) {
  return Object.assign(defaultOptions, returnRealOptions(options))
}

/**
 * 检查输入配置项是否合格
 */
function returnRealOptions(options) {
  // 对象键值校验
  if(isPlainObject(options)){
    return options
  } else {
    return {}
  }
}

/**
 *
 * @param value
 * @returns {boolean}
 */
function isPlainObject(value) {
  if (Object.prototype.toString.call(value) !== '[object Object]') {
    return false;
  }
  const prototype = Object.getPrototypeOf(value);
  return prototype === null || prototype === Object.getPrototypeOf({});
}


/**
 *
 * @param value
 * @returns {boolean}
 */
function isStringNumber(value){
  return (typeof value === 'string' && String(parseInt(value)) === value) || (typeof value === 'number' && value > 0);
}


module.exports = {
  mergeOptions,
  isPlainObject,
  isStringNumber
}
